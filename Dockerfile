FROM debian:bookworm-slim

WORKDIR app/
COPY . .

RUN apt update
RUN apt install curl unzip wget gnupg2 -y

# Install aws-cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

# Add postgres15-client repo
RUN apt install -y postgresql postgresql-contrib

CMD ["sh", "backup.sh"]